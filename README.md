# A frontend code challenge for [XXXXXXX]

This challenge is based on a keypad code.
The user will have 3 attempts to enter it.
The correct code is 1234.
After 3 wrong attempts, the pin pad should be locked out. After 5 seconds, it will allow the entry of attempts again.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run start
```

### Compiles and minifies for production

```
npm run build
```

### Run your unit tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```
