export enum ResultState {
  LOCKED = "LOCKED",
  OK = "OK",
  ERROR = "ERROR",
  RESET = ""
}

export enum PinPadConfig {
  MAX_ATTEMPTS = 3,
  USER_INPUT_FIRED_PROCESS = 4,
  COUNT_DOWN = 5
}

export interface PinPadState {
  result: string;
  userInput: string;
  attempts: number;
  loading: boolean;
}

export const INITIAL_STATE: PinPadState = {
  result: "",
  userInput: "",
  attempts: 0,
  loading: false
};
