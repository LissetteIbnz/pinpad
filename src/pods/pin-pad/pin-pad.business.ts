export const convertUserInput = (str: string = ""): string =>
  str && "*".repeat(str.length - 1) + str.slice(-1);
