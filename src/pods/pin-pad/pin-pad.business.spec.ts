import { convertUserInput } from "./pin-pad.business";

describe("convertUserInput =>", () => {
  it("Should return an empty string when pass an empty string", () => {
    const password = "";
    const expected = "";
    const result = convertUserInput(password);
    expect(result).toBe(expected);
  });

  it("Should return a formatted string", () => {
    const password = "1234";
    const expected = "***4";
    const result = convertUserInput(password);
    expect(result).toBe(expected);
  });

  it("Should return a formatted string (only transform when more than one character passes)", () => {
    const password = "1";
    const expected = "1";
    const result = convertUserInput(password);
    expect(result).toBe(expected);
  });
});
