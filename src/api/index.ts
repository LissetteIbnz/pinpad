const CORRECT_CODE = "1234";
type RESULT_API = "OK" | "ERROR";

export const checkCode = async (code: string): Promise<RESULT_API> => {
  const promise = new Promise<RESULT_API>(resolve => {
    setTimeout(() => resolve(code === CORRECT_CODE ? "OK" : "ERROR"), 1000);
  });

  return promise;
};
